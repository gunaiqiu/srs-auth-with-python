#!/bin/bash
#need change directory info#
cwd=`pwd`
clear
cat $cwd'/'supervisor-srs-auth.ini
echo -e "config file ended.\nDid U checked all the paths?"
read -p '!!!!Make Sure All the Path Suit Your System!!!    y/n :  ' -t 10 flag
if [ "$flag" != "y" ];then
    exit 0
else
    #make client pid directory#
    mkdir -p $cwd/pids
    #install supervisor#
    yum -y install epel-release
    yum -y install supervisor
    systemctl enable supervisord.service
    systemctl start supervisord.service
    #create app config#
    ln -s $cwd/supervisor-srs-auth.ini /etc/supervisord.d/
    if [ "$1" != "--with-out-srs" ];then
        #install SRS#
        yum install docker -y
        systemctl enable docker.service
        systemctl start docker.service
        systemctl status docker.service
        rm -fr $cwd"/srs.log"
        touch $cwd"/srs.log"
        docker run -p 1935:1935 -v $cwd"/srs.conf":/usr/local/srs/conf/srs.conf -v $cwd"/srs.log":/usr/local/srs/objs/srs.log --restart=always ossrs/srs:4
    fi
    #install uWSGI#
    yum -y install python3 python3-libs python3-devel
    pip3 install -r requirements.txt
    #first shot#
    systemctl restart supervisord.service
    sleep 5
    systemctl status supervisord.service
    ps -ef | grep uwsgi| grep -v grep
fi
