import logging
import requests as requests
from flask import Flask, request
from urllib.parse import urlparse

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def main():
    return '-1'


@app.route('/publish', methods=['POST'])
def publish():
    client_request = request.get_json()
    # print(client_request)
    responsecode = '0'
    try:
        client_params = urlparse(client_request['param'])
        token = client_params.query.split("=")[1]
        # print(token)
        if token != 'f89439c8df5b1274b7f613b3f8f4066b':
            kick_url = "http://127.0.0.1:1985/api/v1/clients/" + client_request['client_id']
            # print(kick_url)
            kick_response = requests.delete(kick_url)
            # print(kick_response.text)
            responsecode = '-1'
    except Exception as e:
        logging.exception(e)
    return responsecode


@app.route('/play', methods=['POST'])
def play():
    client_request = request.get_json()
    responsecode = '0'
    # print(client_request)
    try:
        client_params = urlparse(client_request['param'])
        token = client_params.query.split("=")[1]
        # print(token)
        if token != 'b502966ee52ce6bfce573287d59c93c3':
            kick_url = "http://127.0.0.1:1985/api/v1/clients/" + client_request['client_id']
            # print(kick_url)
            kick_response = requests.delete(kick_url)
            # print(kick_response.text)
            responsecode = '-1'
    except Exception as e:
        logging.exception(e)
    return responsecode


if __name__ == '__main__':
    app.run(port=7777)
