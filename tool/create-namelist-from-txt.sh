#!/bin/sh
echo "Prepare name:password:action format list file, e.g."
echo "admin:tuiliu:publish"
echo "admin:laliu:play"
echo "Authorized action: publish , play"
echo "Useage : X.sh name.list out.list"
OLD_IFS="$IFS"
echo "" > $2
for line in `cat $1`
do
  IFS=":"
  array=($line)
  name=${array[0]}
  password_md5=`echo ${array[1]} | md5sum - | cut -d" " -f1`
  action=${array[2]}
  IFS=$OLD_IFS
  echo $name":"$password_md5":"$action >>$2
done
