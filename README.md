# 上手使用指南：
### 环境：CentOS7 + Python3  
#### 一、生成验证列表步骤：
1、在`name-password-action.list`中按照格式，输入相关用户名密码还有操作权限的信息，可以自己抓自己的数据库按照格式生成   
2、运行`create-namelist-from-txt.sh` 输出生成list文件，放到srs-auth.py同级目录下    
默认admin/admin.   
#### 二、修改`supervisor-srs-auth.ini`里面的路径
保证跟你当前路径(最好是你最终要放的地方)对的上
#### 三、修改srs-auth.ini里面的端口和路径
`http=0.0.0.0:7777` (端口)  
`wsgi-file=/root/srs-auth/srs-auth.py` (路径)  
`touch-reload=/root/srs-auth/` (路径)  
#### 四、运行initialize脚本
##### 1、运行参数： 
不要在本机装srs可以加上`--with-out-srs`跳过docker srs的安装
##### 2、默认会安装：
`supervisor`:保证验证服务不挂的  
`docker`:运行srs的  
`srs`:提供rtmp服务的  
`python3`:运行服务脚本的  
`uwsgi`:运行web服务的  
##### 3、srs端口修改
`docker run -p 1935:1935 ..... ossrs/srs:2`  
修改前面那个1935，就是修改宿主机的端口号，改成自己规划的端口。  
另外SRS官网上还有开了几个端口，分别是1985和8080，有需要可自行添加-p参数转发。
#### 五、测试推流拉流
本机推流测试：`rtmp://127.0.0.1/live?do=publish&key=2cccb9bbb7b0f20a916233e38899c0f9/test`  
本机拉流测试：`rtmp://127.0.0.1/live?do=play&key=287d2df531c43e8abb4aab223a9aaa61/test`  
使用前将127.0.0.1改成实际能访问到的IP。
#### 六、现在正在推流和拉流的客户端信息都会在pids文件夹下显示出来。
进去看看吧
#### 七、重启试试
重启后检查下`supervisor`服务、`docker`里面的`srs`服务是否起来了，最后检查下`uwsgi`进程有没有起来。  
检查端口：`7777`，`1935` (自己的改的端口自己定)
#### 八、使用url-generator-open.htm文件，生成推流拉流地址
修改htm里面的`ref()`函数，里面内置了两组服务器。  
我把拉流和推流的地址区分了，可以自行修改。
### TODO:
~~1.开箱即用的脚本，自动搭建SRS(docker)和验证后台环境~~  
2.加密使用的算法为比较固定的用户名密码对比，可以加入时间因素来做防盗链。